package be.kdg.java2.validationtutorial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.Valid;

//You would normally put the WebMvcConfigurer implementation
//in a separate class with an @Configuration attribute
//(see the Converter examples...)
@Controller
public class WebController implements WebMvcConfigurer {
    private static final Logger log = LoggerFactory.getLogger(WebController.class);

    //maps /results to the results view (results.html)
    //use viewcontrollers for your static html pages...
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }


    /*@GetMapping("/")
    public String showForm(PersonForm personForm) {
        log.debug("Running showform...");
        return "form";
    }*/

    //This is the same, but I add the personForm to the model myself...
    @GetMapping("/")
    public String showForm(Model model) {
        model.addAttribute("personForm", new PersonForm());
        return "form";
    }

    /*@PostMapping("/")
    public String checkPersonInfo(@Valid PersonForm personForm, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "form";
        }

        return "redirect:/results";
    }*/

    //I added the Model parameter and added the personForm (=personDTO in our language...)
    //to the model. This is normally done automatically by Spring but
    //I do it to avoid the Intellij errors in de Thymeleaf form...
    @PostMapping("/")
    public String checkPersonInfo(@Valid PersonForm personForm, BindingResult bindingResult, Model model) {
        model.addAttribute("personForm", personForm);
        if (bindingResult.hasErrors()) {
            return "form";
        }

        return "redirect:/results";
    }
}
